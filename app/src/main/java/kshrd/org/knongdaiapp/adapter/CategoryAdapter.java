package kshrd.org.knongdaiapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.activity.WebInfoActivity;
import kshrd.org.knongdaiapp.entities.CategoryViewHolder;
import kshrd.org.knongdaiapp.entities.SubCategory;
import kshrd.org.knongdaiapp.entities.SubCategoryViewHolder;


public class CategoryAdapter extends ExpandableRecyclerViewAdapter<CategoryViewHolder,SubCategoryViewHolder> {

    private Context context;
    private List<String> image;
    public CategoryAdapter(Context context, List<? extends ExpandableGroup> groups, List<String> image) {
        super(groups);
        this.context = context;
        this.image = image;
    }

    @Override
    public CategoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.category_row,parent,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public SubCategoryViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.sub_category_row,parent,false);
        return new SubCategoryViewHolder(view,context);
    }

    @Override
    public void onBindChildViewHolder(SubCategoryViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final SubCategory subCategory= (SubCategory) group.getItems().get(childIndex);
        holder.onBind(subCategory);
        holder.itemView.setOnClickListener(view->{
            Intent intent = new Intent(context, WebInfoActivity.class);
           intent.putExtra("catId",subCategory.getId());
           context.startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.ease_in,R.anim.ease_out);
        });
    }

    @Override
    public void onBindGroupViewHolder(CategoryViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setCategoryName(group);
        String imageurl = image.get(flatPosition >= image.size()? 0 : flatPosition);
        if(null != imageurl && !"".equals(imageurl)){
            Picasso.with(context)
                    .load(Uri.parse(imageurl))
                    .resize(30,40)
                    .centerInside()
                    .into(holder.catImageItem);
        }

    }

}
