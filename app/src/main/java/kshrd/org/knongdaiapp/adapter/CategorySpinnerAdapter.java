package kshrd.org.knongdaiapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import kshrd.org.knongdaiapp.entities.SpinnerDataForCategory;

public class CategorySpinnerAdapter extends BaseAdapter {
    private Context context;
    private List<SpinnerDataForCategory> spinnerData;

    public CategorySpinnerAdapter(Context context,List<SpinnerDataForCategory> spinnerData) {
        this.context = context;
        this.spinnerData = spinnerData;
    }

    @Override
    public int getCount() {
        return spinnerData.size();
    }

    @Override
    public SpinnerDataForCategory getItem(int position) {
        return spinnerData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        SpinnerViewHolder spinnerViewHolder;
        if (view == null){
            view = ((Activity)context).getLayoutInflater().inflate(android.R.layout.simple_list_item_1,parent, false);
            spinnerViewHolder = new SpinnerViewHolder(view);
            view.setTag(spinnerViewHolder);
        }else{
            spinnerViewHolder = (SpinnerViewHolder) view.getTag();
        }
        spinnerViewHolder.textView.setText(spinnerData.get(position).getCatName());
        return view;
    }

    class SpinnerViewHolder{
        TextView textView;
        public SpinnerViewHolder(View v){
            textView = v.findViewById(android.R.id.text1);
        }
    }
}
