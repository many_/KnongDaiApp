package kshrd.org.knongdaiapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.entities.item.WebInfo;

public class WebInfoAdapter extends RecyclerView.Adapter<WebInfoAdapter.WebViewHolder> {
    private Context ctx;
    private List<WebInfo> webSiteInfos;
    public WebInfoAdapter(Context ctx, List<WebInfo> webSiteInfos) {
        this.ctx = ctx;
        this.webSiteInfos = webSiteInfos;
    }

    @Override
    public WebInfoAdapter.WebViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = ((Activity)ctx).getLayoutInflater().inflate(R.layout.web_info_row,parent,false);
        return new WebViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WebInfoAdapter.WebViewHolder holder, int position) {
        holder.tvName.setText(webSiteInfos.get(position).getTitle());
        holder.tvPhone.setText(webSiteInfos.get(position).getPhone());
        holder.tvWebUrl.setText(webSiteInfos.get(position).getLink());
        holder.tvAddress.setText(webSiteInfos.get(position).getAddress());

        String url = webSiteInfos.get(position).getPicUrl();
        if(null != url && !"".equals(url)){
            Picasso.with(ctx)
                    .load(Uri.parse(url))
                    .resize(100,130)
                    .centerInside()
                    .into(holder.imgLogo);
        }

    }

    @Override
    public int getItemCount() {
        return webSiteInfos.size();
    }


    class WebViewHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvPhone, tvWebUrl, tvAddress;
        ImageView imgLogo;
        public WebViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.name);
            tvPhone = itemView.findViewById(R.id.phone);
            tvWebUrl = itemView.findViewById(R.id.website);
            tvAddress = itemView.findViewById(R.id.address);
            imgLogo = itemView.findViewById(R.id.image_view);

            itemView.setOnClickListener(view -> gotoWebUrl(webSiteInfos.get(getAdapterPosition()).getLink()));
        }

        private void gotoWebUrl(String url) {
            try{
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                ctx.startActivity(intent);
                ((Activity)ctx).overridePendingTransition(R.anim.ease_in,R.anim.ease_out);
            }catch (Exception e){
                Toast.makeText(ctx,"Invalid URL.....",Toast.LENGTH_SHORT).show();
            }

        }
    }
}
