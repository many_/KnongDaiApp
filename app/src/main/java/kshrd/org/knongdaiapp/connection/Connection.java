package kshrd.org.knongdaiapp.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
public class Connection {

    public static boolean isConnected(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if(connectivityManager != null){
            networkInfo = connectivityManager.getActiveNetworkInfo();
            if(networkInfo != null){
                if(networkInfo.isConnected()){
                    return true;
                }
            }
        }
        return false;
    }
}
