package kshrd.org.knongdaiapp.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.greenrobot.eventbus.EventBus;
import kshrd.org.knongdaiapp.entities.ConnectionStateEvent;
public class NetworkChangeReciever extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, final Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if(Connection.isConnected(context)){
                EventBus.getDefault().post(new ConnectionStateEvent(1));
            }else{
                EventBus.getDefault().post(new ConnectionStateEvent(0));
            }
        }
    }
}