package kshrd.org.knongdaiapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kshrd.org.knongdaiapp.activity.KnongDaiActivity;

public class SplashScreen extends AppCompatActivity{
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.knongdai_splash_screen);
        Flowable.interval(1, TimeUnit.SECONDS)
                .timeInterval()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .takeUntil(integer->integer.value() >= 5)
                .map(integer-> dotString(integer.value()))
                .subscribe(((TextView)findViewById(R.id.loading))::setText,
                        err-> Log.e("error",err.getMessage()),
                        ()->{
                            Intent intent = new Intent(SplashScreen.this, KnongDaiActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.ease_in,R.anim.ease_out);
                            finish();
                        }
                );
    }


    private String dotString(long times){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<times;i++){
            stringBuilder.append(".");
        }
        return stringBuilder.toString();
    }
}
