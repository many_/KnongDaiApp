
package kshrd.org.knongdaiapp.entities.item;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseWebInfo {

    @SerializedName("code")
    private String mCode;
    @SerializedName("data")
    private List<WebInfo> mData;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("pagination")
    private Pagination mPagination;
    @SerializedName("status")
    private Boolean mStatus;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public List<WebInfo> getData() {
        return mData;
    }

    public void setData(List<WebInfo> data) {
        mData = data;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination pagination) {
        mPagination = pagination;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
