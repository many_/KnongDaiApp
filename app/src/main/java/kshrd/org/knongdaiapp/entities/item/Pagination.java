
package kshrd.org.knongdaiapp.entities.item;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Pagination {

    @SerializedName("current_page")
    private Long mCurrentPage;
    @SerializedName("total_page")
    private Long mTotalPage;
    @SerializedName("total_record")
    private Long mTotalRecord;

    public Long getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(Long currentPage) {
        mCurrentPage = currentPage;
    }

    public Long getTotalPage() {
        return mTotalPage;
    }

    public void setTotalPage(Long totalPage) {
        mTotalPage = totalPage;
    }

    public Long getTotalRecord() {
        return mTotalRecord;
    }

    public void setTotalRecord(Long totalRecord) {
        mTotalRecord = totalRecord;
    }

}
