
package kshrd.org.knongdaiapp.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category implements Parcelable{

    @SerializedName("cate_name")
    private String mCateName;
    @SerializedName("des")
    private String mDes;
    @SerializedName("icon_name")
    private String mIconName;
    @SerializedName("id")
    private Long mId;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("sub_cate")
    private List<SubCategory> mSubCategory;

    protected Category(Parcel in) {
        mCateName = in.readString();
        mDes = in.readString();
        mIconName = in.readString();
        if (in.readByte() == 0) {
            mId = null;
        } else {
            mId = in.readLong();
        }
        byte tmpMStatus = in.readByte();
        mStatus = tmpMStatus == 0 ? null : tmpMStatus == 1;
        mSubCategory = in.createTypedArrayList(SubCategory.CREATOR);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getCateName() {
        return mCateName;
    }

    public void setCateName(String cateName) {
        mCateName = cateName;
    }

    public String getDes() {
        return mDes;
    }

    public void setDes(String des) {
        mDes = des;
    }

    public String getIconName() {
        return mIconName;
    }

    public void setIconName(String iconName) {
        mIconName = iconName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public List<SubCategory> getSubCate() {
        return mSubCategory;
    }

    public void setSubCate(List<SubCategory> subCategory) {
        mSubCategory = subCategory;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCateName);
        dest.writeString(mDes);
        dest.writeString(mIconName);
        if (mId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(mId);
        }
        dest.writeByte((byte) (mStatus == null ? 0 : mStatus ? 1 : 2));
        dest.writeTypedList(mSubCategory);
    }

    @Override
    public String toString() {
        return "Category{" +
                "mCateName='" + mCateName + '\'' +
                ", mDes='" + mDes + '\'' +
                ", mIconName='" + mIconName + '\'' +
                ", mId=" + mId +
                ", mStatus=" + mStatus +
                ", mSubCategory=" + mSubCategory +
                '}';
    }
}
