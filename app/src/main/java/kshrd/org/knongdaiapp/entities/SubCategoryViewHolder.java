package kshrd.org.knongdaiapp.entities;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import kshrd.org.knongdaiapp.R;

public class SubCategoryViewHolder extends ChildViewHolder {
    public TextView subCategory;
    private ImageView subCatImage;
    private Context context;
    public SubCategoryViewHolder(View itemView, Context context) {
        super(itemView);
        subCategory = itemView.findViewById(R.id.sub_category);
        subCatImage = itemView.findViewById(R.id.sub_category_image);
        this.context = context;
    }

    public void onBind(SubCategory subCategory) {
        this.subCategory.setText(subCategory.getCateName());
        String url = subCategory.getIconName();
        if(null != url && !"".equals(url)){
            Picasso.with(context)
                    .load(Uri.parse(url))
                    .into(this.subCatImage);
        }
    }

}
