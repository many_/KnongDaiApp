
package kshrd.org.knongdaiapp.entities.form;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class UserForm {

    @SerializedName("email")
    private String mEmail;
    @SerializedName("first_password")
    private String mFirstPassword;
    @SerializedName("id")
    private Long mId;
    @SerializedName("role_id")
    private Long mRoleId;
    @SerializedName("second_password")
    private String mSecondPassword;
    @SerializedName("user_name")
    private String mUserName;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFirstPassword() {
        return mFirstPassword;
    }

    public void setFirstPassword(String firstPassword) {
        mFirstPassword = firstPassword;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getRoleId() {
        return mRoleId;
    }

    public void setRoleId(Long roleId) {
        mRoleId = roleId;
    }

    public String getSecondPassword() {
        return mSecondPassword;
    }

    public void setSecondPassword(String secondPassword) {
        mSecondPassword = secondPassword;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    @Override
    public String toString() {
        return "UserForm{" +
                "mEmail='" + mEmail + '\'' +
                ", mFirstPassword='" + mFirstPassword + '\'' +
                ", mId=" + mId +
                ", mRoleId=" + mRoleId +
                ", mSecondPassword='" + mSecondPassword + '\'' +
                ", mUserName='" + mUserName + '\'' +
                '}';
    }
}
