package kshrd.org.knongdaiapp.entities;

/**
 * Created by ITE_33 on 30-Nov-17.
 */

public class SpinnerDataForCategory {
    private long mId;
    private String catName;

    public SpinnerDataForCategory(long mId, String catName) {
        this.mId = mId;
        this.catName = catName;
    }

    public long getmId() {
        return mId;
    }

    public void setmId(long mId) {
        this.mId = mId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @Override
    public String toString() {
        return "SpinnerDataForCategory{" +
                "mId=" + mId +
                ", catName='" + catName + '\'' +
                '}';
    }
}
