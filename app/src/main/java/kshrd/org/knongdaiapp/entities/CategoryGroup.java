package kshrd.org.knongdaiapp.entities;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;


public class CategoryGroup extends ExpandableGroup<SubCategory> {
    public CategoryGroup(String title, List<SubCategory> items) {
        super(title, items);
    }
}
