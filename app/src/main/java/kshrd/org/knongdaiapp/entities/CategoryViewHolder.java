package kshrd.org.knongdaiapp.entities;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import kshrd.org.knongdaiapp.R;


public class CategoryViewHolder extends GroupViewHolder {
    public TextView category;
    private ImageView catImage;
    public ImageView catImageItem;
    public CategoryViewHolder(View itemView) {
        super(itemView);
        category = itemView.findViewById(R.id.category);
        catImage = itemView.findViewById(R.id.category_image);
        catImageItem = itemView.findViewById(R.id.image_item);
    }

    public void setCategoryName(ExpandableGroup group) {
        category.setText(group.getTitle());
    }

    @Override
    public void expand() {
        super.expand();
        catImage.setImageResource(android.R.drawable.arrow_up_float);
    }

    @Override
    public void collapse() {
        super.collapse();
        catImage.setImageResource(android.R.drawable.arrow_down_float);
    }
}
