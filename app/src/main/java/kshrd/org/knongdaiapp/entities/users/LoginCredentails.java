package kshrd.org.knongdaiapp.entities.users;

/**
 * Created by many on 11/30/17.
 */

public class LoginCredentails {
    private String email;
    private String password;

    public LoginCredentails(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
