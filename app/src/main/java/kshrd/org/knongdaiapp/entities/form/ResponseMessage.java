
package kshrd.org.knongdaiapp.entities.form;
import com.google.gson.annotations.SerializedName;

public class ResponseMessage {

    @SerializedName("code")
    private String mCode;

    @SerializedName("data")
    private String mData;

    @SerializedName("msg")
    private String mMsg;

    @SerializedName("status")
    private Boolean mStatus;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    @Override
    public String toString() {
        return "ResponseMessage{" +
                "mCode='" + mCode + '\'' +
                ", mData='" + mData + '\'' +
                ", mMsg='" + mMsg + '\'' +
                ", mStatus=" + mStatus +
                '}';
    }
}
