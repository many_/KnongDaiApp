package kshrd.org.knongdaiapp.entities;

public class ConnectionStateEvent {
    private int connected;

    public ConnectionStateEvent(int connected) {
        this.connected = connected;
    }

    public int getConnected() {
        return connected;
    }

    public void setConnected(int connected) {
        this.connected = connected;
    }
}
