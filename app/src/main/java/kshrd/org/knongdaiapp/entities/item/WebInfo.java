
package kshrd.org.knongdaiapp.entities.item;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class WebInfo {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("des")
    private String mDes;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("link")
    private String mLink;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("pic_url")
    private String mPicUrl;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("type")
    private String mType;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDes() {
        return mDes;
    }

    public void setDes(String des) {
        mDes = des;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPicUrl() {
        return mPicUrl;
    }

    public void setPicUrl(String picUrl) {
        mPicUrl = picUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    @Override
    public String toString() {
        return "WebInfo{" +
                "mAddress='" + mAddress + '\'' +
                ", mDes='" + mDes + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mId=" + mId +
                ", mLink='" + mLink + '\'' +
                ", mPhone='" + mPhone + '\'' +
                ", mPicUrl='" + mPicUrl + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mType='" + mType + '\'' +
                '}';
    }
}
