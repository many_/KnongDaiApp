
package kshrd.org.knongdaiapp.entities.form;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AddWebsiteForm {
    @SerializedName("address")
    private String mAddress;
    @SerializedName("approved")
    private Boolean mApproved;
    @SerializedName("des")
    private String mDes;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("keywords")
    private List<String> mKeywords;
    @SerializedName("link")
    private String mLink;
    @SerializedName("main_cate_id")
    private Long mMainCateId;
    @SerializedName("nullIfStringEmpty")
    private String mNullIfStringEmpty;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("pic_url")
    private String mPicUrl;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("sub_cate_id")
    private Long mSubCateId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("type")
    private String mType;
    @SerializedName("userId")
    private Long mUserId;

    public AddWebsiteForm(String mAddress, Boolean mApproved, String mDes, String mEmail, Long mId, List<String> mKeywords, String mLink, Long mMainCateId, String mNullIfStringEmpty, String mPhone, String mPicUrl, Boolean mStatus, Long mSubCateId, String mTitle, String mType, Long mUserId) {
        this.mAddress = mAddress;
        this.mApproved = mApproved;
        this.mDes = mDes;
        this.mEmail = mEmail;
        this.mId = mId;
        this.mKeywords = mKeywords;
        this.mLink = mLink;
        this.mMainCateId = mMainCateId;
        this.mNullIfStringEmpty = mNullIfStringEmpty;
        this.mPhone = mPhone;
        this.mPicUrl = mPicUrl;
        this.mStatus = mStatus;
        this.mSubCateId = mSubCateId;
        this.mTitle = mTitle;
        this.mType = mType;
        this.mUserId = mUserId;
    }

    public AddWebsiteForm() {}

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Boolean getApproved() {
        return mApproved;
    }

    public void setApproved(Boolean approved) {
        mApproved = approved;
    }

    public String getDes() {
        return mDes;
    }

    public void setDes(String des) {
        mDes = des;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public List<String> getKeywords() {
        return mKeywords;
    }

    public void setKeywords(List<String> keywords) {
        mKeywords = keywords;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public Long getMainCateId() {
        return mMainCateId;
    }

    public void setMainCateId(Long mainCateId) {
        mMainCateId = mainCateId;
    }

    public String getNullIfStringEmpty() {
        return mNullIfStringEmpty;
    }

    public void setNullIfStringEmpty(String nullIfStringEmpty) {
        mNullIfStringEmpty = nullIfStringEmpty;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPicUrl() {
        return mPicUrl;
    }

    public void setPicUrl(String picUrl) {
        mPicUrl = picUrl;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public Long getSubCateId() {
        return mSubCateId;
    }

    public void setSubCateId(Long subCateId) {
        mSubCateId = subCateId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }


    @Override
    public String toString() {
        return "AddWebsiteForm{" +
                "mAddress='" + mAddress + '\'' +
                ", mApproved=" + mApproved +
                ", mDes='" + mDes + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mId=" + mId +
                ", mKeywords=" + mKeywords +
                ", mLink='" + mLink + '\'' +
                ", mMainCateId=" + mMainCateId +
                ", mNullIfStringEmpty='" + mNullIfStringEmpty + '\'' +
                ", mPhone='" + mPhone + '\'' +
                ", mPicUrl='" + mPicUrl + '\'' +
                ", mStatus=" + mStatus +
                ", mSubCateId=" + mSubCateId +
                ", mTitle='" + mTitle + '\'' +
                ", mType='" + mType + '\'' +
                ", mUserId=" + mUserId +
                '}';
    }
}
