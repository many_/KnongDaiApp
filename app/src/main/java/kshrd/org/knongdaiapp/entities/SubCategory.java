
package kshrd.org.knongdaiapp.entities;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class SubCategory implements Parcelable {

    @SerializedName("cate_name")
    private String mCateName;
    @SerializedName("des")
    private String mDes;
    @SerializedName("icon_name")
    private String mIconName;
    @SerializedName("id")
    private Long mId;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("total_url")
    private Long mTotalUrl;


    public SubCategory(String mCateName, String mDes, String mIconName, Long mId, Boolean mStatus, Long mTotalUrl) {
        this.mCateName = mCateName;
        this.mDes = mDes;
        this.mIconName = mIconName;
        this.mId = mId;
        this.mStatus = mStatus;
        this.mTotalUrl = mTotalUrl;
    }
    public SubCategory(){}

    protected SubCategory(Parcel in) {
        mCateName = in.readString();
        mDes = in.readString();
        mIconName = in.readString();
        if (in.readByte() == 0) {
            mId = null;
        } else {
            mId = in.readLong();
        }
        byte tmpMStatus = in.readByte();
        mStatus = tmpMStatus == 0 ? null : tmpMStatus == 1;
        if (in.readByte() == 0) {
            mTotalUrl = null;
        } else {
            mTotalUrl = in.readLong();
        }
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public String getCateName() {
        return mCateName;
    }

    public void setCateName(String cateName) {
        mCateName = cateName;
    }

    public String getDes() {
        return mDes;
    }

    public void setDes(String des) {
        mDes = des;
    }

    public String getIconName() {
        return mIconName;
    }

    public void setIconName(String iconName) {
        mIconName = iconName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public Long getTotalUrl() {
        return mTotalUrl;
    }

    public void setTotalUrl(Long totalUrl) {
        mTotalUrl = totalUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCateName);
        dest.writeString(mDes);
        dest.writeString(mIconName);
        if (mId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(mId);
        }
        dest.writeByte((byte) (mStatus == null ? 0 : mStatus ? 1 : 2));
        if (mTotalUrl == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(mTotalUrl);
        }
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "mCateName='" + mCateName + '\'' +
                ", mDes='" + mDes + '\'' +
                ", mIconName='" + mIconName + '\'' +
                ", mId=" + mId +
                ", mStatus=" + mStatus +
                ", mTotalUrl=" + mTotalUrl +
                '}';
    }
}
