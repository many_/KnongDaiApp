package kshrd.org.knongdaiapp.activity;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.adapter.WebInfoAdapter;
import kshrd.org.knongdaiapp.api.CategoryApi;
import kshrd.org.knongdaiapp.connection.Connection;
import kshrd.org.knongdaiapp.entities.ConnectionStateEvent;
import kshrd.org.knongdaiapp.entities.item.WebInfo;
import kshrd.org.knongdaiapp.retrofit.ServiceGenerator;

public class WebInfoActivity extends AppCompatActivity implements ServiceGenerator.OnConnectionTimeoutListener{
    private RecyclerView recyclerView;
    private WebInfoAdapter dataAdapter;
    private List<WebInfo> dataList;
    private MaterialSearchView materialSearchView;
    private static int currentPage = 1;
    private  boolean isInitDataByCategory = true;
    private String currentKeyWord = "";
    private  long catId;
    private ProgressDialog progressBar;
    private LinearLayout connectionStateLayout;
    private CategoryApi categoryApi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_info);
        initToolBar();
        connectionStateLayout = findViewById(R.id.connection_state);
        progressDialog();
        dataList = new ArrayList<>();
        dataAdapter = new WebInfoAdapter(this, dataList);
        catId = getIntent().getLongExtra("catId", 0);
        initRecyclerView();
        checkConnection(Connection.isConnected(this));
        categoryApi = ServiceGenerator.createService(this, CategoryApi.class, null);
        initData(currentPage, catId);
    }

    private void initToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        materialSearchView = findViewById(R.id.material_search);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.in_your_hand));


        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(null != query && !"".equals(query)){
                    currentPage = 1;
                    initData(currentPage, query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(null != newText && !"".equals(newText)){
                    categoryApi.getSuggestionByKeyWord(newText)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(suggestion->{
                                materialSearchView.setSuggestions(suggestion);
                            });
                }
                return false;
            }
        });
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager layoutManager;
        if(getResources().getBoolean(R.bool.isLargeScreen) || getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ){
            layoutManager = new GridLayoutManager(this,2);
        }else{
            layoutManager= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(dataAdapter);
    }

    private void initData(int page, long categoryId){
        if(connectionStateLayout.getVisibility() == View.GONE){
            progressBar.show();
            isInitDataByCategory = true;
            currentKeyWord = "";
            categoryApi.getAllWebInfoByCategoryId(page,categoryId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(webinfo->{
                               /* dataList = webinfo.getData();
                                dataAdapter = new WebInfoAdapter(this,dataList);
                                recyclerView.setAdapter(dataAdapter);*/
                               dataList.clear();
                               dataList.addAll(webinfo.getData());
                               dataAdapter.notifyDataSetChanged();
                                initPagination(Integer.parseInt(String.valueOf(webinfo.getPagination().getTotalPage())));

                            },
                            throwable -> progressBar.dismiss(),
                            ()->progressBar.dismiss());
        }
    }
    private void initData(int page, String keyword){
        if(connectionStateLayout.getVisibility() == View.GONE) {
            progressBar.show();
            isInitDataByCategory = false;
            currentKeyWord = keyword;
            categoryApi.getAllWebInfoByKeyword(keyword, page)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(webinfo -> {
                               /* dataList = webinfo.getData();

                                dataAdapter = new WebInfoAdapter(this, dataList);
                                recyclerView.setAdapter(dataAdapter);*/
                                dataList.clear();
                                dataList.addAll(webinfo.getData());
                                dataAdapter.notifyDataSetChanged();
                                initPagination(Integer.parseInt(String.valueOf(webinfo.getPagination().getTotalPage())));
                            },
                            throwable -> progressBar.dismiss(),
                            () -> progressBar.dismiss());
        }
    }

    private void initPagination(int size) {
        LinearLayout layout = findViewById(R.id.paginationLayout);
        layout.removeAllViews();
        Observable.range(1,size)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s->{
                    Button btnTag = new Button(this);
                    initPagingButton(btnTag, s);
                    layout.addView(btnTag);
                });
    }

    private void initPagingButton(Button btnTag, Integer s) {
        btnTag.setText(String.valueOf(s));
        btnTag.setWidth(20);
        btnTag.setHeight(30);
        btnTag.setBackgroundColor(currentPage == s?getResources().getColor(R.color.colorPrimary) :getResources().getColor(R.color.cardview_dark_background));
        btnTag.setTextColor(getResources().getColor(android.R.color.white));
        btnTag.setOnClickListener(view->{
            view.setBackgroundColor(getResources().getColor(R.color.cardview_dark_background));
            currentPage = s;
            if(isInitDataByCategory){
                initData(currentPage, catId);
            }else{
                initData(currentPage,currentKeyWord);
            }
        });
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.ease_in, R.anim.ease_out);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.knong_dai_search,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        materialSearchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    //onStop
    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void checkConnection(boolean  connected){
        if(connected){
            connectionStateLayout.setVisibility(View.GONE);

        }else{
            connectionStateLayout.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onConnectionStateRecieve(ConnectionStateEvent event){
        checkConnection(event.getConnected() > 0);
    }

    private void progressDialog(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Waiting ...");
        progressBar.setMax(100);
    }

    @Override
    public void onConnectionTimeout() {
        if(progressBar.isShowing()) progressBar.dismiss();
        Toast.makeText(this, "Connection Timeout...", Toast.LENGTH_LONG).show();
    }
}
