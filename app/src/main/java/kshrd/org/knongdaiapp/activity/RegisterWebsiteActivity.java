package kshrd.org.knongdaiapp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.adapter.CategorySpinnerAdapter;
import kshrd.org.knongdaiapp.api.CategoryApi;
import kshrd.org.knongdaiapp.connection.Connection;
import kshrd.org.knongdaiapp.credentail.LoginCredentailPreferences;
import kshrd.org.knongdaiapp.entities.Category;
import kshrd.org.knongdaiapp.entities.ConnectionStateEvent;
import kshrd.org.knongdaiapp.entities.ResponseCategory;
import kshrd.org.knongdaiapp.entities.SpinnerDataForCategory;
import kshrd.org.knongdaiapp.entities.SubCategory;
import kshrd.org.knongdaiapp.entities.form.AddWebsiteForm;
import kshrd.org.knongdaiapp.entities.form.ResponseMessage;
import kshrd.org.knongdaiapp.entities.users.LoginCredentails;
import kshrd.org.knongdaiapp.retrofit.ServiceGenerator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
public class RegisterWebsiteActivity extends AppCompatActivity implements ServiceGenerator.OnConnectionTimeoutListener{

    private Spinner spinnerCategory, spinnerSubCategory;
    private ProgressDialog progressBar;
    private LinearLayout connectionStateLayout;
    private Button btnChooseImage;
    private EditText etWebTitle,etUrl,etPhone,etMail,etAddress,etImagUrl, etDescription,etKeyWord;
    private RadioButton rdFacebook;
    private FloatingActionButton btnSave;
    private final int REQUEST_IMAGE_CODE = 90;
    private CategoryApi categoryApi;
    private LoginCredentails credential;
    private final int PERMISSION_REQUEST_CODE = 91;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_website);

        spinnerCategory = findViewById(R.id.spinCategory);
        spinnerSubCategory = findViewById(R.id.spinSubCategory);
        connectionStateLayout = findViewById(R.id.connection_state);
        checkConnection(Connection.isConnected(this));

        credential = LoginCredentailPreferences.getCredential(this);
        categoryApi = ServiceGenerator.createService(this, CategoryApi.class, credential);
        progressDialog();
        getCategoryToSpinner(spinnerCategory);
        initWidget();

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getSubCategoryToSpinner(spinnerSubCategory,((SpinnerDataForCategory)spinnerCategory.getSelectedItem()).getCatName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initWidget() {
        btnChooseImage = findViewById(R.id.btnChooseImage);
        btnSave = findViewById(R.id.btnSave);
        etWebTitle = findViewById(R.id.etWebTitle);
        etUrl = findViewById(R.id.etUrl);
        etPhone = findViewById(R.id.etPhone);
        etMail = findViewById(R.id.etMail);
        etAddress = findViewById(R.id.etAddress);
        etImagUrl = findViewById(R.id.etImagUrl);
        etDescription = findViewById(R.id.etDescription);
        etKeyWord = findViewById(R.id.etKeyWord);

        rdFacebook = findViewById(R.id.rdFacebook);

        //Do action
        btnChooseImage.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQUEST_IMAGE_CODE);
                }else{
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                }
            }else{
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_IMAGE_CODE);
            }
        });

        btnSave.setOnClickListener(v -> {
            progressBar.show();
            saveNewWebsite();
            progressBar.dismiss();
        });
    }
    private void saveNewWebsite() {

        boolean isApprove = true;
        boolean status = true;
        long userId = 1L;
        Long id = 0L; // Url Id is generated by system
        String imageUrl = etImagUrl.getText().toString();
        List<String> keyword = getListForKeyword(etKeyWord.getText().toString());

        AddWebsiteForm addWebsiteForm = new AddWebsiteForm();
        addWebsiteForm.setAddress(etAddress.getText().toString());
        addWebsiteForm.setApproved(isApprove);
        addWebsiteForm.setDes(etDescription.getText().toString());
        addWebsiteForm.setEmail(etMail.getText().toString());
        addWebsiteForm.setUserId(userId);
        addWebsiteForm.setId(id);
        addWebsiteForm.setKeywords(keyword);
        addWebsiteForm.setLink(etUrl.getText().toString());
        addWebsiteForm.setMainCateId(1L);
        addWebsiteForm.setSubCateId(2L);
        addWebsiteForm.setNullIfStringEmpty("NullIfEmpty");
        addWebsiteForm.setPhone(etPhone.getText().toString());
        addWebsiteForm.setPicUrl("".equals(imageUrl) ? "" : ServiceGenerator.BASE_IMAGE_URL + imageUrl);
        addWebsiteForm.setStatus(status);
        addWebsiteForm.setTitle(etWebTitle.getText().toString());
        addWebsiteForm.setType(rdFacebook.isChecked()? "f" : "w");

        categoryApi.addNewWebsite(addWebsiteForm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item->{},throwable -> {Log.e("Error Inertd URL", throwable.toString());},()->{
                    Toast.makeText(this, "Successfully Inserted....", Toast.LENGTH_SHORT).show();
                    if(progressBar.isShowing()) progressBar.dismiss();
                    super.onBackPressed();
                });
    }
    private List<String> getListForKeyword(String keyword){
        List<String> keywordList = new ArrayList<>();
        String[] key = keyword.split(",");
        for(String k : key){
            if(!"".equals(k) && !" ".equals(k)){
                keywordList.add(k.trim());
            }
        }
        return keywordList;
    }

    private void getCategoryToSpinner(Spinner spinner){
        progressBar.show();
        categoryApi.getAllCategoryWithSubCategory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(responseCategory ->{
                    List<Category> categoryList = responseCategory.getData();
                    List<SpinnerDataForCategory> SpinnerData = new ArrayList<>();
                    for(Category category : categoryList){
                        SpinnerData.add(new SpinnerDataForCategory(category.getId(), category.getCateName()));
                    }
                    return SpinnerData;
                } )
                .subscribe(catName->{
                    CategorySpinnerAdapter arrayAdapter = new CategorySpinnerAdapter(this,catName);
                    spinner.setAdapter(arrayAdapter);
                },
                        throwable -> {Log.e("Error At Category",throwable.toString());progressBar.dismiss();},
                        ()->{
                            if(!spinnerCategory.getAdapter().isEmpty()){
                                getSubCategoryToSpinner(spinnerSubCategory,((SpinnerDataForCategory)spinnerCategory.getItemAtPosition(0)).getCatName());
                            }
                            progressBar.dismiss();
                        } );


    }
    private void getSubCategoryToSpinner(Spinner spinner, String categoryName){
        categoryApi.getAllCategoryWithSubCategory()
                .subscribeOn(Schedulers.io())
                .map(ResponseCategory::getData)
                .flatMapIterable(categories -> categories)
                .filter(category -> category.getCateName().equals(categoryName))
                .toList()
                .map(categories -> {
                   List<SubCategory> subCategoryList = categories.get(0).getSubCate();
                   List<SpinnerDataForCategory> subName = new ArrayList<>();
                   for(SubCategory subCategory : subCategoryList){
                       subName.add(new SpinnerDataForCategory(subCategory.getId(), subCategory.getCateName()));
                   }
                   return subName;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subcategory->{
                    CategorySpinnerAdapter arrayAdapter = new CategorySpinnerAdapter(this,subcategory);
                    spinner.setAdapter(arrayAdapter);
                });


    }
    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    //onStop
    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onConnectionStateRecieve(ConnectionStateEvent event){
        checkConnection(event.getConnected() > 0);
    }
    public void checkConnection(boolean  connected){
        if(connected){
            connectionStateLayout.setVisibility(View.GONE);
        }else{
            connectionStateLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CODE && resultCode == RESULT_OK){
            Uri uri = data.getData();
            uploadFile(uri,etImagUrl);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_REQUEST_CODE && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE) && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_IMAGE_CODE);
        }

    }

    private void progressDialog(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Waiting ...");
        progressBar.setMax(100);
    }

    private void uploadFile(Uri fileUri, EditText editText) {
        progressBar.show();
        String path =  getRealPathFromURI(fileUri);
        File file = new File(path);

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

       Observable<ResponseMessage> uploadObservable = categoryApi.uploadWebIcon(name, body);
        uploadObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody-> {
                    if(responseBody!= null){
                        if(responseBody.getStatus()){
                            editText.setText(responseBody.getData());
                        }
                    }else{
                        Log.e("Error Upload File ",responseBody.toString());
                    }
                },
                throwable ->{ Log.e("Error Upload File ",throwable.toString());progressBar.dismiss();},
                ()->progressBar.dismiss());
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onConnectionTimeout() {
        if(progressBar.isShowing()) progressBar.dismiss();
        Toast.makeText(this, "Connection Timeout...", Toast.LENGTH_LONG).show();
    }
}
