package kshrd.org.knongdaiapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.adapter.CategoryAdapter;
import kshrd.org.knongdaiapp.api.CategoryApi;
import kshrd.org.knongdaiapp.connection.Connection;
import kshrd.org.knongdaiapp.entities.Category;
import kshrd.org.knongdaiapp.entities.CategoryGroup;
import kshrd.org.knongdaiapp.entities.ConnectionStateEvent;
import kshrd.org.knongdaiapp.entities.ResponseCategory;
import kshrd.org.knongdaiapp.entities.SubCategory;
import kshrd.org.knongdaiapp.retrofit.ServiceGenerator;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

public class KnongDaiActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,ServiceGenerator.OnConnectionTimeoutListener{

    private MaterialSearchView searchView;
    private List<String> catImage = new ArrayList<>();
    private LinearLayout connectionStateLayout;
    private ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knong_dai);
        progressDialog();

        connectionStateLayout = findViewById(R.id.connection_state);
        //searchView = findViewById(R.id.searchView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        checkConnection(Connection.isConnected(this));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.ease_in, R.anim.ease_out);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    private void getCategoryGroup() {
        progressBar.show();
        ServiceGenerator.createService(this, CategoryApi.class, null)
                .getAllCategoryWithSubCategory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                 .map(ResponseCategory::getData)
                .subscribe(this::initCategoryGroup,
                throwable -> {
                    Log.e("permission",throwable.toString());
                    progressBar.dismiss();
                },
                ()->progressBar.dismiss());
    }

    private void initCategoryGroup(List<Category> category) {
        List<CategoryGroup> categoryGroupList = new ArrayList<>();
        for (Category cat : category){
            String imageUrl = cat.getIconName();
            catImage.add(null == imageUrl? "" : imageUrl);
            List<SubCategory> subCategoryList = cat.getSubCate();

            if(null != subCategoryList){
                if(!subCategoryList.isEmpty()){
                    categoryGroupList.add(new CategoryGroup(cat.getCateName(), cat.getSubCate()));
                }
            }
        }
        initRecyclerView(categoryGroupList);
    }

    private void initRecyclerView(List<CategoryGroup> categoryGroups) {

        RecyclerView recyclerView = findViewById(R.id.recycler_view_category);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        DividerItemDecoration itemDecor = new DividerItemDecoration(this, VERTICAL);
        recyclerView.addItemDecoration(itemDecor);

        //instantiate your adapter with the list of genres
        CategoryAdapter adapter = new CategoryAdapter(this, categoryGroups,catImage );
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void checkConnection(boolean  connected){
        if(connected){
            connectionStateLayout.setVisibility(View.GONE);
            getCategoryGroup();
        }else{
            connectionStateLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    //onStop
    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onConnectionStateRecieve(ConnectionStateEvent event){
        checkConnection(event.getConnected() > 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.knong_dai_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                Intent intent =new Intent(this,RegisterWebsiteActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.ease_in, R.anim.ease_out);
                return true;
             default:
                    return super.onOptionsItemSelected(item);
        }

    }

    private void progressDialog(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Waiting ...");
    }

    @Override
    public void onConnectionTimeout() {
        Toast.makeText(this, "Connection Timeout...", Toast.LENGTH_LONG).show();
    }
}
