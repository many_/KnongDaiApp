package kshrd.org.knongdaiapp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import kshrd.org.knongdaiapp.R;
import kshrd.org.knongdaiapp.api.UserApi;
import kshrd.org.knongdaiapp.entities.users.LoginCredentails;
import kshrd.org.knongdaiapp.retrofit.ServiceGenerator;

/**
 * Created by many on 11/30/17.
 */

public class LoginDialog  extends DialogFragment{
    private Context context;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.login_form, null, false);
        EditText etEmail = view.findViewById(R.id.email);
        EditText etPassword = view.findViewById(R.id.password);

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Create User")
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("បង្កើត", (dialog, which) -> {
                    String email = etEmail.getText().toString().trim();
                    String password = etPassword.getText().toString().trim();
                    if("".equals(email) || "".equals(password)){
                        Toast.makeText(context, "Email and Password are required...", Toast.LENGTH_SHORT).show();
                    }else{
                        LoginCredentails credentails = new LoginCredentails(email, password);
                        ServiceGenerator.createService(context, UserApi.class, credentails);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("បោះបង់", (dialog, which) -> {
                    dialog.dismiss();
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
