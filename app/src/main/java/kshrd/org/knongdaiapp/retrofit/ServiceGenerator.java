package kshrd.org.knongdaiapp.retrofit;

import android.content.Context;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import kshrd.org.knongdaiapp.entities.users.LoginCredentails;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    private static final String BASE_URL = "http://110.74.194.125:15000/";
    public static final String BASE_IMAGE_URL = "http://110.74.194.125:15000";
    private static OnConnectionTimeoutListener onConnectionTimeoutListener;

    private static Retrofit getRetrofit(String baseUrl, LoginCredentails credentails){

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(credentails == null ? getOkHttpClientWithNoCredential() : getOkHttpClient(credentails))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static  <T> T createService(Context context, Class<T> className, LoginCredentails credentials){
        if(context instanceof OnConnectionTimeoutListener){
            onConnectionTimeoutListener = (OnConnectionTimeoutListener)context;
        }
        return getRetrofit(BASE_URL, credentials).create(className);
    }

    private static OkHttpClient getOkHttpClientWithNoCredential(){
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    try{
                        Request request = chain.request();

                        Request.Builder builder = request.newBuilder();
                        Request newRequest = builder.build();
                        return chain.proceed(newRequest);
                    }catch (SocketTimeoutException e){
                        onConnectionTimeoutListener.onConnectionTimeout();
                    }
                    return chain.proceed(chain.request());
                })
                .build();
    }
    private static OkHttpClient getOkHttpClient(LoginCredentails credentails){
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    try{
                        Request request = chain.request();

                        Request.Builder builder = request
                                .newBuilder()
                                .header("Authorization", Credentials.basic(credentails.getEmail(),credentails.getPassword()));
                        Request newRequest = builder.build();
                        return chain.proceed(newRequest);
                    }catch (SocketTimeoutException e){
                        onConnectionTimeoutListener.onConnectionTimeout();
                    }
                    return chain.proceed(chain.request());
                })
                .build();
    }
    public interface OnConnectionTimeoutListener {
        void onConnectionTimeout();
    }
}
