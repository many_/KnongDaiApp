package kshrd.org.knongdaiapp.credentail;

import android.content.Context;
import android.content.SharedPreferences;

import kshrd.org.knongdaiapp.entities.users.LoginCredentails;

/**
 * Created by many on 11/30/17.
 */

public class LoginCredentailPreferences {
    private static SharedPreferences.Editor editor;

    private static SharedPreferences.Editor getSharedPreferences(Context context){
       if(editor == null){
           SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
           editor = sharedPreferences.edit();
       }
        return editor;
    }

    public static void saveLoginPreferences(Context context, LoginCredentails credentails){
        getSharedPreferences(context).putString("email",credentails.getEmail());
        getSharedPreferences(context).putString("password", credentails.getPassword());
        getSharedPreferences(context).apply();
    }

    public static LoginCredentails getCredential(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email", "");
        String password = sharedPreferences.getString("password", "");
        return new LoginCredentails(email, password);
    }
}
