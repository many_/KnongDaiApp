package kshrd.org.knongdaiapp.api;

import io.reactivex.Observable;
import kshrd.org.knongdaiapp.entities.ResponseCategory;
import kshrd.org.knongdaiapp.entities.form.AddWebsiteForm;
import kshrd.org.knongdaiapp.entities.form.ResponseMessage;
import kshrd.org.knongdaiapp.entities.item.ResponseWebInfo;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CategoryApi {
    @GET("api/v1/categories")
    Observable<ResponseCategory> getAllCategoryWithSubCategory();


    @GET("api/v1/search")
    Observable<ResponseWebInfo> getAllWebInfoByCategoryId(@Query("page") int page, @Query("cate_id") long pageId);

   @GET("api/v1/search/keyword/{keyword}")
    Observable<String[]> getSuggestionByKeyWord(@Path("keyword") String keyWord);

    @GET("api/v1/search")
    Observable<ResponseWebInfo> getAllWebInfoByKeyword(@Query("q") String keyword,@Query("page") int page);

    @POST("api/v1/urls/create")
    Observable<ResponseBody> addNewWebsite(@Body AddWebsiteForm websiteForm);

    @Multipart
    @POST("api/v1/urls/upload/web-icon")
    Observable<ResponseMessage> uploadWebIcon(@Part("description")RequestBody description, @Part MultipartBody.Part file);

}
