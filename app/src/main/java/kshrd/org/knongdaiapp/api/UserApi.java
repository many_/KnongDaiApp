package kshrd.org.knongdaiapp.api;


import io.reactivex.Observable;
import kshrd.org.knongdaiapp.entities.form.ResponseUserByEmail;
import kshrd.org.knongdaiapp.entities.form.UserForm;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserApi {

    @POST("api/v1/users/create")
    Observable<UserForm> createUser(@Body UserForm userForm);

    @GET("/api/v1/users/find-by-email")
    Observable<ResponseUserByEmail> findUserByEmail(@Query("email") String email);
}
